package org.springframework.samples.petclinic.visit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.samples.petclinic.vet.Vet;

public class VisitTest {

	public static Visit visit;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		visit = new Visit();
	}

	@Test
	public void testVisit() {
		assertNotNull(visit);				
	}
	
	@Test
	public void testVet() {
		
		assertNull(visit.getVet());
		
		Vet vet = new Vet();
		vet.setFirstName("Juan Manuel");
		vet.setLastName("Murillo");		
		visit.setVet(vet);
		
		assertNotNull(visit.getVet());
		assertEquals(vet,visit.getVet());
	}
}
