package org.springframework.samples.petclinic.vet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.samples.petclinic.visit.Visit;

public class VetTest {

	public static Vet vet;
	public static Vet vet2;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		vet = new Vet();
		vet.setFirstName("Juan Manuel");
		vet.setLastName("Murillo");
	}
	
	@Test
	public void testVet() {
		assertNotNull(vet);				
	}
	
	@Test
	public void testVet2() {
		assertNull(vet2);				
	}
	
	 @Test
	    public void addVisitTest() {
	    	
	    	List<Visit> list = new LinkedList<Visit>();
	    	Visit visit = new Visit();
	    	visit.setDate(LocalDate.now());
	    	visit.setDescription("Descripcion");
	    	visit.setId(888);
	    	visit.setPetId(1);
	    	visit.setVet(vet);
	    	
	    	//List is empty
	    	assertEquals(list,vet.getVisits());
	    	
	    	vet.addVisit(visit);
	    	list.add(visit);
	    	
		assertNotNull(vet.getVisits());
	    	assertEquals(list,vet.getVisits());
	    }
	
}